var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);


var player,backgroundPlayer;
var done = false;

function playAudioBackground(lien, debut){
    if (backgroundPlayer)
        backgroundPlayer.destroy();
    backgroundPlayer = new YT.Player('background-player', {
        height: '1',
        width: '1',
        videoId: lien, 
        playerVars: {
            autoplay: 1,
            loop: 1,
            start: debut
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange,
            'onError': onError
        }
    });
}

function playAudio(lien, debut) {
    if (player)
        player.destroy();
    player = new YT.Player('player', {
        height: '1',
        width: '1',
        videoId: lien, 
        playerVars: {
            autoplay: 1,
            loop: 1,
            start: debut
        },
        events: {
            'onReady': onPlayerReady,
            'onStateChange': onPlayerStateChange,
            'onError': onError
        }
    });
}

function onError(event) {
    document.querySelector(".loader-musique").style.display = "inline-block";
    pause = 1;
    cliquable = 0;
    if(fini != 1){
        console.log(event)
        next_question();
    }
}

function onPlayerReady(event) {
    document.querySelector(".loader-musique").style.display = "inline-block";
    pause = 1;
    cliquable = 0;
    event.target.playVideo();
}

function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING) {
        document.querySelector(".loader-musique").style.display = "none";
        pause = 0;
        cliquable = 1;
    }
}
function stopVideo() {
    if(player)
    player.stopVideo();
}
function pauseVideo() {
    if(player)
    player.pauseVideo();
}
function playVideo() {
    if(player)
    player.playVideo();
}

function pauseBackground() {
    if(backgroundPlayer)
    backgroundPlayer.pauseVideo();
}
function playBackground() {
    if(backgroundPlayer)
    backgroundPlayer.playVideo();
}


//playAudio("IDNLsHkHliU",45);