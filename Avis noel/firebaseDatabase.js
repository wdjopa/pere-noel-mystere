/*
// Your web app's Firebase configuration
var firebaseConfig = {
    apiKey: "AIzaSyAYtj3mqX75SdBjqVJLDlgwLv8_DbhqNxo",
    authDomain: "airbus-a9d0f.firebaseapp.com",
    databaseURL: "https://airbus-a9d0f.firebaseio.com",
    projectId: "airbus-a9d0f",
    storageBucket: "airbus-a9d0f.appspot.com",
    messagingSenderId: "53525693589",
    appId: "1:53525693589:web:7098aa0c1b0838122ec79c"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
*/

var database = firebase.database();

function writeData(userId, datas) {
    userId = userId.replace(/[.]/g, '____');
    database.ref('articles/' + userId).update(datas);
    return true;
}
/**
 * Fonction qui permet d'ajouter un élément dans la bdd
 * @param {String} dbnameString 
 * @param {Object} datas 
 */
function pushData(dbnameString, datas) {
    // userId = userId.replace(/[.]/g, '____');
    return database.ref(dbnameString).push(datas);
}
/**
 * Fonction qui permet d'ajouter un élément dans la bdd
 * @param {String} dbnameString 
 * @param {Object} datas 
 */
function setData(dbnameString, datas) {
    // userId = userId.replace(/[.]/g, '____');
    return database.ref(dbnameString).set(datas);
}

function writeUserData(userId, name, imageUrl, exclus, password, recu, donne) {
    userId = userId.replace(/[.]/g, '____');
    database.ref('articles/' + userId).set({
        username: name,
        email: userId.replace(/____/g, '.'),
        profile_picture: imageUrl,
        exclus: exclus,
        password: password,
        recoit: recu,
        donne: donne
    });

    return true;
}

function updateUserData(userId, updates) {

    userId = userId.replace(/[.]/g, '____');
    for (let i in updates) {
        let update = updates[i]
        console.log(userId)
        console.log(updates[i])
        database.ref('articles/' + userId).update(update);
    }
}

function readData(name) {
    return database.ref('/' + name).once('value').then(function (snapshot) {
        return snapshot.val()
    });
}

function checkConnexion(email, pass){
    let r = false;
    email = email.toLowerCase().trim().replace(/[.]/g,'____');
    database.ref('users/' + (new Date()).getFullYear() + '/' +email).once('value').then(function(snapshot) {
        let user = snapshot.val();
        if(user){
            console.log(user)
            console.log(user.password == pass)
            console.log(user.password)
            console.log(pass)
            if(user.password == pass){
                return user;
            }
            else{
                return null;
            }
        }
        else{
            return null;
        }
    }).then(function (a){
        if(a!=null){
            theUserData = a;
            user = email;
            fine(true)
        }
        else{
            bad()
        }
    });
}

function checkConnexionByCookie(email){
    let r = false;
    email = email.toLowerCase().trim().replace(/[.]/g,'____');
    database.ref('users/' + (new Date()).getFullYear() + '/' +email).once('value').then(function(snapshot) {
        let user = snapshot.val();
        if(user){
            return true;
        }
        else{
            return false;
        }
    }).then(function (a){
        if(a){
            setTimeout(() => {
                fine()
            }, 2000);
        }
        else{
            setTimeout(() => {
                bad()
            }, 2000);
        }
    });
}



function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function deleteCookie(cname) {
    document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC; path=/;";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        if (user != "" && user != null) {
            setCookie("username", user, 365);
        }
    }
}